#!/bin/sh
$JAVA_HOME/bin/native-image \
--report-unsupported-elements-at-runtime  \
--enable-all-security-services \
-jar target/lab-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
releases/lab-1.0.0-linux --no-server --static
