# lab

## Requirements

- Install GraalVM
- Install native-image module

### Linux

```bash
sudo apt-get update
sudo apt-get install build-essential libz-dev -y
wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.0.0/graalvm-ce-java11-linux-amd64-20.0.0.tar.gz
tar -xvzf graalvm-ce-java11-linux-amd64-20.0.0.tar.gz
rm graalvm-ce-java11-linux-amd64-20.0.0.tar.gz
export JAVA_HOME=$HOME/graalvm-ce-java11-20.0.0
export PATH=$PATH:$JAVA_HOME/bin
gu install native-image
```

### OSX

```bash
xcode-select --install
wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.0.0/graalvm-ce-java11-darwin-amd64-20.0.0.tar.gz
tar -xvzf graalvm-ce-java11-darwin-amd64-20.0.0.tar.gz
rm graalvm-ce-java11-darwin-amd64-20.0.0.tar.gz
export JAVA_HOME=$HOME/graalvm-ce-java11-20.0.0
export PATH=$PATH:$JAVA_HOME/bin
gu install native-image
```

## Build

### First build jar file

```bash
mvn clean package
```

### If you're under Linux

```bash
$JAVA_HOME/bin/native-image \
--report-unsupported-elements-at-runtime  \
--enable-all-security-services \
-jar target/lab-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
releases/lab-1.0.0-linux --no-server --static
```


### If you're under OSX

```bash
$JAVA_HOME/bin/native-image \
--report-unsupported-elements-at-runtime  \
--enable-all-security-services \
-jar target/lab-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
releases/lab-1.0.0-darwin --no-server
```

### If you're under OSX and want to build a linux release

```bash
cd vm.linux/build
vagrant up
vagrant provision --provision-with build
vagrant provision --provision-with tests
vagrant halt
```

## Use the sample

```bash
export GITLAB_TOKEN="your-token"
./releases/lab-1.0.0-darwin shuffle
./releases/lab-1.0.0-darwin user --user-name k33g
./releases/lab-1.0.0-darwin projects --user-name k33g


./releases/lab-1.0.0-darwin --help
./releases/lab-1.0.0-darwin shuffle --help
./releases/lab-1.0.0-darwin user --help
```

- https://github.com/oracle/graal/issues/407
- https://hub.docker.com/r/oracle/graalvm-ce/
- https://www.infoq.com/articles/java-native-cli-graalvm-picocli/