#!/bin/sh
$JAVA_HOME/bin/native-image \
--report-unsupported-elements-at-runtime  \
--enable-all-security-services \
-jar target/lab-1.0.0-SNAPSHOT-jar-with-dependencies.jar \
releases/lab-1.0.0-darwin --no-server

./releases/lab-1.0.0-darwin shuffle
./releases/lab-1.0.0-darwin user --user-name k33g