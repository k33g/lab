package garden.bots.lab.tools

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import java.util.*

class Shell {
  // execute a shell command
  fun exec(command: String): Either<Exception, ShellExecutionResult> {
    return try {
      val proc = Runtime.getRuntime().exec(command)
      val output: StringBuilder = StringBuilder()
      Scanner(proc.inputStream).use {
        while (it.hasNextLine()) {
          val line = it.nextLine()
          //println(line)
          output.append(line)
        }
      }
      Right(ShellExecutionResult(output = output, exitCode = proc.exitValue()))
    } catch (exception: Exception) {
      Left(exception)
    }
  }
}