package garden.bots.lab.tools

import arrow.core.*
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest

class GitLab {
  fun token(): Option<String> {
    return Option.fromNullable(System.getenv("GITLAB_TOKEN"))
  }

  fun url(): Option<String> {
    return Option.fromNullable(System.getenv("GITLAB_URL"))
  }

  fun get(path: String): Either<String, String> {
    val url = url()
    val urlApi = when (url) {
      is None -> "https://gitlab.com/api/v4"
      is Some -> "${url.t}/api/v4"
    }
    token().let {
      when (it) {
        is None -> {
          return Left("GITLAB_TOKEN is empty")
        }
        is Some -> {
          //println("😃 ${urlApi}")
          //TODO: pagination
          val token = it.t
          try {
            val client: HttpClient = HttpClient.newHttpClient()
            val request = HttpRequest.newBuilder()
              .uri(URI.create("${urlApi}${path}"))
              .header("accept", "application/json")
              .header("Private-Token", token)
              .build()
            val result = client.send(request, java.net.http.HttpResponse.BodyHandlers.ofString()).body()
            return Right(result)
          } catch (exception: Exception) {
            return Left(exception.cause.toString())
          }
        }
      }
    }
  }


}
/*
client.sendAsync(request, java.net.http.HttpResponse.BodyHandlers.ofString())
        .thenApply { response -> response.body() }
        .thenAccept { body ->
            println(body)
        }
        .join()
 */