package garden.bots.lab.tools

data class ShellExecutionResult(val output: StringBuilder, val exitCode: Int) {

}