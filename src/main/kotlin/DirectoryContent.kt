package garden.bots.lab

import arrow.core.Either
import com.github.ajalt.clikt.core.CliktCommand
import garden.bots.lab.tools.Shell

class DirectoryContent : CliktCommand(help = "ls command of the current directory - only on osx and linux") {
  override fun run() {
    // how to check the OS?
    // how to check if git or another component is installed?

    Shell().exec("ls").let {
      when (it) {
        is Either.Left -> println("😡 ${it.a.message}")
        is Either.Right -> println(it.b.output.toString())
      }
    }
  }
}