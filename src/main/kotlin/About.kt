package garden.bots.lab

import com.github.ajalt.clikt.core.CliktCommand

class About : CliktCommand(help="about lab") {
    override fun run() {
        echo("🦊 About Lab...")
    }
}