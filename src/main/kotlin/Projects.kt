package garden.bots.lab

import arrow.core.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import garden.bots.lab.tools.GitLab

/*
 GET /users/:user_id/projects
 https://docs.gitlab.com/ee/api/projects.html#list-user-projects
 Get a list of visible projects owned by the given user. When accessed without authentication, only public projects are returned.
 lab projects --user-name k33g
 */
class Projects : CliktCommand(help = "get projects list of a user") {
  private val userName: String? by option(help = "name of the user: --user-name bob")
  private val userId: String? by option(help = "name of the user: --user-id 1212")
  private val query: String? by option(help = "wip 🚧")
  private val format: String? by option(help = "wip 🚧")

  private fun userNameOrId(): Option<String> {
    return Option.fromNullable(userName).orElse { Option.fromNullable(userId) }
  }

  override fun run() {
    userNameOrId().let { it ->
      when (it) {
        is None -> {
          echo("😡 user name or id is empty")
        }
        is Some -> {
          val user = it.t
          GitLab().get("/users/${user}/projects").let {
            when (it) {
              is Either.Left -> echo("😡 ${it.a}")
              is Either.Right -> echo(it.b)
            }
          }
        }
      }
    }

  }
}