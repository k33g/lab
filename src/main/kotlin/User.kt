package garden.bots.lab

import arrow.core.Either
import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import garden.bots.lab.tools.GitLab

class User : CliktCommand(help = "get user information") {
  private val userName: String? by option(help = "name of the user: --user-name bob")
  private val query: String? by option(help = "wip 🚧")
  private val format: String? by option(help = "wip 🚧")

  private fun userName(): Option<String> {
    return Option.fromNullable(userName)
  }

  override fun run() {

    userName().let { it ->
      when (it) {
        is None -> {
          echo("😡 user name is empty")
        }
        is Some -> {
          val user = it.t
          GitLab().get("/users?username=${user}").let {
            when (it) {
              is Either.Left -> echo("😡 ${it.a}")
              is Either.Right -> echo(it.b)
            }
          }
        }
      }
    }
  }
}
/*
    val jsonArray: JsonArray<JsonObject> = parser.parse(StringBuilder(body)) as JsonArray<JsonObject>
    println(jsonArray[0]["username"])
*/