package garden.bots.lab

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.github.ajalt.clikt.core.CliktCommand

fun giveMeSomething(): Either<String, Int> {
  return when ((0..2).shuffled().first()) {
    0 -> Left("🤭 Oups! I did it again")
    1 -> Right(42)
    else -> Right(666)
  }
}

class Shuffle : CliktCommand(help = "get a number") {
  override fun run() {
    giveMeSomething().let {
      when (it) {
        is Either.Left -> echo("😡 ${it.a}")
        is Either.Right -> echo("🎉 ${it.b}")
      }
    }
  }
}


