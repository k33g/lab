package garden.bots.lab

import com.github.ajalt.clikt.core.subcommands

// https://ajalt.github.io/clikt/quickstart/#basic-concepts

fun main(args: Array<String>) = Lab().subcommands(
  Shuffle(),
  User(),
  About(),
  DirectoryContent(),
  Projects()
).main(args)





