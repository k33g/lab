#!/bin/sh
cd vm.linux/build
vagrant up
vagrant provision --provision-with build
vagrant provision --provision-with tests
vagrant halt
